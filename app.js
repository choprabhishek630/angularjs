(function(){
 	var app = angular.module('userLogin', [ ]);
 	app.controller('userValidate',['$http',function($http){
 		this.email='';
 		this.pwd='';
 		var user=this;
 		this.register=function(){
 			$http({
  				method: 'POST',
  				url: 'https://alpha-api.circlein.io/api/user/userLogin',
  				data: {email: this.email, password: this.pwd}
			}).then(function successCallback(response) {
			 	console.log(response);
		 		 localStorage.setItem("access", response.data.data.access_token);
		 		 localStorage.setItem("email", response.config.data.email);
		 		 localStorage.setItem("password", response.config.data.password);
		 		 localStorage.setItem("b_id", response.data.data.business_id);
		 		 window.location = "dashboard.html";

	  		}, function errorCallback(response) {
  				alert('incorrect username or password');
  			});
 		}
 	}]);
})();